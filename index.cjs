const express = require('express')
const app = express()
const port = 5001

app.get('/',(request,response)=>{
    response.send("Checking Demo Server")
})
app.listen(port,()=>{
    console.log(`listening app gives http://localhost:${port}`)
})
